﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addmigrations : DbMigration
    {
        public override void Up()
        {
            DropColumn("public.cars_models", "version");
        }
        
        public override void Down()
        {
            AddColumn("public.cars_models", "version", c => c.String());
        }
    }
}
