﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class titlelocationcity : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.advertisement", "location_city", c => c.String());
            AddColumn("public.advertisement", "outskirts", c => c.Boolean(nullable: false));
            AddColumn("public.advertisement", "title", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("public.advertisement", "title");
            DropColumn("public.advertisement", "outskirts");
            DropColumn("public.advertisement", "location_city");
        }
    }
}
