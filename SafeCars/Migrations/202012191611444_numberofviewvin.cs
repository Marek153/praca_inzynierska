﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class numberofviewvin : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.advertisement", "number_of_views_vin", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("public.advertisement", "number_of_views_vin");
        }
    }
}
