﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class banUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.users", "ban_message", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.users", "ban_message");
        }
    }
}
