﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carM : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.cars_models", "version", c => c.String());
            DropColumn("public.cars_models", "brand");
        }
        
        public override void Down()
        {
            AddColumn("public.cars_models", "brand", c => c.String());
            DropColumn("public.cars_models", "version");
        }
    }
}
