﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carModels : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "public.cars_models", newName: "car_models_model");
            MoveTable(name: "public.car_models_model", newSchema: "dbo");
        }
        
        public override void Down()
        {
            MoveTable(name: "dbo.car_models_model", newSchema: "public");
            RenameTable(name: "public.car_models_model", newName: "cars_models");
        }
    }
}
