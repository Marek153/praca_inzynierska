﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mileage : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.car", "mileage", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("public.car", "mileage");
        }
    }
}
