﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carM5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.cars_models", "id_cars_makes", c => c.Int(nullable: false));
            CreateIndex("public.cars_models", "id_cars_makes");
            AddForeignKey("public.cars_models", "id_cars_makes", "public.car_makes", "id_cars_makes", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("public.cars_models", "id_cars_makes", "public.car_makes");
            DropIndex("public.cars_models", new[] { "id_cars_makes" });
            DropColumn("public.cars_models", "id_cars_makes");
        }
    }
}
