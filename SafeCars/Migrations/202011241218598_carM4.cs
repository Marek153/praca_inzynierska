﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carM4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.car_makes",
                c => new
                    {
                        id_cars_makes = c.Int(nullable: false, identity: true),
                        nameMake = c.String(),
                    })
                .PrimaryKey(t => t.id_cars_makes);
            
        }
        
        public override void Down()
        {
            DropTable("public.car_makes");
        }
    }
}
