﻿// <auto-generated />
namespace SafeCars.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class carM3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(carM3));
        
        string IMigrationMetadata.Id
        {
            get { return "202011241214534_carM3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
