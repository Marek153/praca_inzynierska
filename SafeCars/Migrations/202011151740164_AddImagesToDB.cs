﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImagesToDB : DbMigration
    {
        public override void Up()
        {
                Sql("ALTER TABLE Images DROP COLUMN img");
            Sql("ALTER TABLE Images ADD img bytea");
            AlterColumn("public.images", "img", c => c.Binary());
        }
        
        public override void Down()
        {
            AlterColumn("public.images", "img", c => c.Short(nullable: false));
        }
    }
}
