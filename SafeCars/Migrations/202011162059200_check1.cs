﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class check1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("public.users", "phone", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("public.users", "phone", c => c.Int(nullable: false));
        }
    }
}
