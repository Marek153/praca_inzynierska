﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changeinuser : DbMigration
    {
        public override void Up()
        {
            AlterColumn("public.users", "password", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("public.users", "password", c => c.String(nullable: false));
        }
    }
}
