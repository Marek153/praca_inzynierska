﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserModels : DbMigration
    {
        public override void Up()
        {
            AlterColumn("public.users", "email", c => c.String(nullable: false));
            AlterColumn("public.users", "password", c => c.String(nullable: false));
            AlterColumn("public.users", "first_name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("public.users", "city", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("public.users", "city", c => c.String());
            AlterColumn("public.users", "first_name", c => c.String());
            AlterColumn("public.users", "password", c => c.String());
            AlterColumn("public.users", "email", c => c.String());
        }
    }
}
