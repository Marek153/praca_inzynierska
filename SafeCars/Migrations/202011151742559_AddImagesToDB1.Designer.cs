﻿// <auto-generated />
namespace SafeCars.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class AddImagesToDB1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddImagesToDB1));
        
        string IMigrationMetadata.Id
        {
            get { return "202011151742559_AddImagesToDB1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
