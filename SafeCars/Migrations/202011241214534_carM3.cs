﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carM3 : DbMigration
    {
        public override void Up()
        {

            CreateTable(
                "public.car_makes",
                c => new
                    {
                        id_car_makes = c.Int(nullable: false, identity: true),
                        name = c.String(),
                       
                    })
                .PrimaryKey(t => t.id_car_makes);
             
        }
        
        public override void Down()
        {
        }
    }
}
