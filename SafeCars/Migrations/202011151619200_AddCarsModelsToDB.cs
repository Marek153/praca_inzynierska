﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCarsModelsToDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.advertisement",
                c => new
                    {
                        id_advertisement = c.Int(nullable: false, identity: true),
                        id_car = c.Int(nullable: false),
                        id_user = c.Int(nullable: false),
                        location = c.String(),
                        description = c.String(),
                        date_of_issue = c.DateTime(nullable: false),
                        update_date = c.DateTime(nullable: false),
                        number_of_views = c.Int(nullable: false),
                        private_ad = c.Boolean(nullable: false),
                        price = c.Int(nullable: false),
                        active = c.Boolean(nullable: false),
                        time_of_possession = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id_advertisement)
                .ForeignKey("public.car", t => t.id_car, cascadeDelete: true)
                .ForeignKey("public.users", t => t.id_user, cascadeDelete: true)
                .Index(t => t.id_car)
                .Index(t => t.id_user);
            
            CreateTable(
                "public.car",
                c => new
                    {
                        id_car = c.Int(nullable: false, identity: true),
                        id_cars_models = c.Int(nullable: false),
                        year = c.Int(nullable: false),
                        capacity = c.Int(nullable: false),
                        power = c.Int(nullable: false),
                        fuel = c.String(),
                        transmission = c.String(),
                        vin = c.String(),
                        color = c.String(),
                        used = c.Boolean(nullable: false),
                        damaged = c.Boolean(nullable: false),
                        first_registration = c.DateTime(nullable: false),
                        license_plate = c.String(),
                    })
                .PrimaryKey(t => t.id_car)
                .ForeignKey("public.cars_models", t => t.id_cars_models, cascadeDelete: true)
                .Index(t => t.id_cars_models);
            
            CreateTable(
                "public.cars_models",
                c => new
                    {
                        id_cars_models = c.Int(nullable: false, identity: true),
                        brand = c.String(),
                        model = c.String(),
                        version = c.String(),
                    })
                .PrimaryKey(t => t.id_cars_models);
            
            CreateTable(
                "public.users",
                c => new
                    {
                        id_user = c.Int(nullable: false, identity: true),
                        email = c.String(),
                        password = c.String(),
                        first_name = c.String(),
                        city = c.String(),
                        phone = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id_user);
            
            CreateTable(
                "public.complaints",
                c => new
                    {
                        id_complaints = c.Int(nullable: false, identity: true),
                        id_advertisement = c.Int(nullable: false),
                        description = c.String(),
                        id_user = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id_complaints)
                .ForeignKey("public.advertisement", t => t.id_advertisement, cascadeDelete: true)
                .ForeignKey("public.users", t => t.id_user, cascadeDelete: true)
                .Index(t => t.id_advertisement)
                .Index(t => t.id_user);
            
            CreateTable(
                "public.images",
                c => new
                    {
                        id_image = c.Int(nullable: false, identity: true),
                        img = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.id_image);
            
            CreateTable(
                "public.list_images ",
                c => new
                    {
                        id_image = c.Int(nullable: false),
                        id_car = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id_image, t.id_car })
                .ForeignKey("public.car", t => t.id_car, cascadeDelete: true)
                .ForeignKey("public.images", t => t.id_image, cascadeDelete: true)
                .Index(t => t.id_image)
                .Index(t => t.id_car);
            
            CreateTable(
                "public.list_of_ads",
                c => new
                    {
                        id_advertisement = c.Int(nullable: false),
                        id_user = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id_advertisement, t.id_user })
                .ForeignKey("public.advertisement", t => t.id_advertisement, cascadeDelete: true)
                .ForeignKey("public.users", t => t.id_user, cascadeDelete: true)
                .Index(t => t.id_advertisement)
                .Index(t => t.id_user);
            
            CreateTable(
                "public.watchlist",
                c => new
                    {
                        id_advertisement = c.Int(nullable: false),
                        id_user = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id_advertisement, t.id_user })
                .ForeignKey("public.advertisement", t => t.id_advertisement, cascadeDelete: true)
                .ForeignKey("public.users", t => t.id_user, cascadeDelete: true)
                .Index(t => t.id_advertisement)
                .Index(t => t.id_user);
            
        }
        
        public override void Down()
        {
            DropForeignKey("public.watchlist", "id_user", "public.users");
            DropForeignKey("public.watchlist", "id_advertisement", "public.advertisement");
            DropForeignKey("public.list_of_ads", "id_user", "public.users");
            DropForeignKey("public.list_of_ads", "id_advertisement", "public.advertisement");
            DropForeignKey("public.list_images ", "id_image", "public.images");
            DropForeignKey("public.list_images ", "id_car", "public.car");
            DropForeignKey("public.complaints", "id_user", "public.users");
            DropForeignKey("public.complaints", "id_advertisement", "public.advertisement");
            DropForeignKey("public.advertisement", "id_user", "public.users");
            DropForeignKey("public.advertisement", "id_car", "public.car");
            DropForeignKey("public.car", "id_cars_models", "public.cars_models");
            DropIndex("public.watchlist", new[] { "id_user" });
            DropIndex("public.watchlist", new[] { "id_advertisement" });
            DropIndex("public.list_of_ads", new[] { "id_user" });
            DropIndex("public.list_of_ads", new[] { "id_advertisement" });
            DropIndex("public.list_images ", new[] { "id_car" });
            DropIndex("public.list_images ", new[] { "id_image" });
            DropIndex("public.complaints", new[] { "id_user" });
            DropIndex("public.complaints", new[] { "id_advertisement" });
            DropIndex("public.car", new[] { "id_cars_models" });
            DropIndex("public.advertisement", new[] { "id_user" });
            DropIndex("public.advertisement", new[] { "id_car" });
            DropTable("public.watchlist");
            DropTable("public.list_of_ads");
            DropTable("public.list_images ");
            DropTable("public.images");
            DropTable("public.complaints");
            DropTable("public.users");
            DropTable("public.cars_models");
            DropTable("public.car");
            DropTable("public.advertisement");
        }
    }
}
