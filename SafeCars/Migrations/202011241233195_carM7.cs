﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class carM7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.car_makes", "name_make", c => c.String());
        
        }
        
        public override void Down()
        {
            AddColumn("public.car_makes", "namemake", c => c.String());
          
        }
    }
}
