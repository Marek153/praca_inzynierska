﻿namespace SafeCars.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class check2 : DbMigration
    {
        public override void Up()
        {
            MoveTable(name: "public.users", newSchema: "dbo");
        }
        
        public override void Down()
        {
            MoveTable(name: "dbo.users", newSchema: "public");
        }
    }
}
