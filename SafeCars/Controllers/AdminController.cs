﻿using SafeCars.DataContext;
using SafeCars.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SafeCars.Controllers
{
    public class AdminController : Controller
    {
        private DBDataContext db = new DBDataContext();
        [Authorize]
        public ActionResult Index(int? mess)
        {
            if (mess != null)
            {
                if (mess == 1)
                {
                    ViewBag.mess = "Pojazd został dodany!";
                }
                else if (mess == 2)
                {
                    ViewBag.mess = "Użytkownik został zablokowany!";
                }
                else if (mess == 3)
                {
                    ViewBag.mess = "Anulowano skargi dotyczące tego ogłszenia!";
                }
                else if (mess == 4)
                {
                    ViewBag.mess = "Wiadomość została wysłana!";
                }
            }
            return View();
        }
        [AllowAnonymous]
        public ActionResult AddNewCar()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddNewCar(MakeandModel model)
        {
            if (string.IsNullOrEmpty(model.carMake.name_make) || string.IsNullOrEmpty(model.carModel.model))
            {
                ModelState.AddModelError("carModel.model", "Podaj dokładnie dane");
                return View();
            }
            CarMakes carMake = db.CarsMakes.FirstOrDefault(y => y.name_make.ToUpper() == model.carMake.name_make.ToUpper());
            CarMakes newCarMake = new CarMakes();
            CarsModelsModels newCarModel = new CarsModelsModels();
            int id = db.CarsModels.Count();
            // newCarModel.id_cars_models = id + 1;
            newCarModel.id_cars_makes = 1;
            //db.CarsModels.Add(newCarModel);
            //  db.SaveChanges();
            if (carMake == null)
            {
                ModelState.AddModelError("carMake.name_make", "Podaj markę z bazy");
                return View();
            }
            else
            {
                newCarModel.carMakes = carMake;
                newCarModel.id_cars_makes = carMake.id_cars_makes;
            }

            newCarModel.model = model.carModel.model;
            if (string.IsNullOrEmpty(model.carModel.version))
            {
                newCarModel.version = null;
            }
            else
            {
                newCarModel.version = model.carModel.version;
            }

            //int id = db.CarsModels.Count();
            //newCarModel.id_cars_models = id + 1;
            CarsModelsModels test = db.CarsModels.FirstOrDefault(y => y.id_cars_models == newCarModel.id_cars_models);
            //newCarModel.carMakes=newCarMake;
            //  db.Entry(newCarModel).State = EntityState.Added;
            db.CarsModels.Add(newCarModel);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException.Message);
            }
            return RedirectToAction("Index", "Admin", new { mess = 1 });
        }
        [Authorize]
        public ActionResult ListComplaints()
        {
            List<ComplaintsModels> listComplaints = db.Complaints.ToList();
            return View(listComplaints);
        }
        [Authorize]
        public ActionResult BanUser(int id)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            UsersModels user = db.Users.FirstOrDefault(y => y.id_user == adv.id_user);
            user.ban_message = "delete";
            user.ConfirmPassword = user.password;

            DeleteAdv(adv.id_advertisement);
            try
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return RedirectToAction("Index", "Admin", new { mess = 2 });

        }

        public string DeleteAdv(int id)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            db.WatchList.RemoveRange(db.WatchList.Where(x => x.id_advertisement == id));
            List<ListImagesModels> listImage = db.ListImages.Where(y => y.id_car == adv.id_car).ToList();
            foreach (var item in listImage)
            {
                db.Images.RemoveRange(db.Images.Where(y => y.id_image == item.id_image));
            }
            db.ListImages.RemoveRange(listImage);
            db.Car.RemoveRange(db.Car.Where(y => y.id_car == adv.id_car));
            db.ListOfAds.RemoveRange(db.ListOfAds.Where(y => y.id_advertisement == id));
            db.Complaints.RemoveRange(db.Complaints.Where(y => y.id_advertisement == id));

            db.Advertisment.Remove(adv);
            db.SaveChanges();
            return "{\"msg\":\"success\"}";
        }
        [Authorize]
        public ActionResult DeleteComp(int id)
        {
            db.Complaints.RemoveRange(db.Complaints.Where(y => y.id_advertisement == id));
            db.SaveChanges();
            return RedirectToAction("Index", "Admin", new { mess = 3 });
        }
        [Authorize]
        [HttpGet]
        public ActionResult SendBanMessage(int id)
        {

            ComplaintsModels model = new ComplaintsModels();
            model.id_advertisement = id;
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public ActionResult SendBanMessage(ComplaintsModels model)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == model.id_advertisement);
            adv.active = false;
            UsersModels user = db.Users.FirstOrDefault(y => y.id_user == adv.id_user);
            user.ban_message = model.description + "\n    Tytuł ogłoszenia: " + adv.title;
            user.ConfirmPassword = user.password;
            try
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            db.Complaints.RemoveRange(db.Complaints.Where(y => y.id_advertisement == model.id_advertisement));
            db.SaveChanges();
            return RedirectToAction("Index", "Admin", new { mess = 3 });
        }
    }
}