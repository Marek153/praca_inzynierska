﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SafeCars.DataContext;
using SafeCars.Models;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Validation;
using System.Web.Security;
using SafeCars.ExternalApi;

namespace SafeCars.Controllers
{
    public class UsersModelsController : Controller
    {
        private DBDataContext db = new DBDataContext();


        public ActionResult Index()
        {
            if (Session["idUser"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Create");
            }
        }


        // GET: UsersModels/Create
        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Include = "id_user,email,password,ConfirmPassword,first_name,city,phone")] UsersModels usersModels)
        {
            var check = db.Users.FirstOrDefault(s => s.email == usersModels.email);

            if (check == null)
            {
                if (usersModels.password != usersModels.ConfirmPassword)
                {

                    ViewBag.error = "Podane hasła nie są identyczne.";
                    return View();
                }
                usersModels.password = GetMD5(usersModels.password);
                usersModels.ban_message = "empty";
                db.Users.Add(usersModels);

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                catch (InvalidOperationException e)
                {

                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.error = "Ten emeil juz jest w naszej bazie.";
                return View();
            }


        }
        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string email, string password)
        {
            if (ModelState.IsValid)
            {
                var f_password = GetMD5(password);
                var data = db.Users.Where(s => s.email.Equals(email) && s.password.Equals(f_password)).ToList();
                if (data.Count() > 0)
                {
                    if (data.FirstOrDefault().ban_message == "delete")
                    {
                        ModelState.AddModelError("password", "Twoje konto zostało zablokowane");
                        return View();
                    }
                    //add session
                    Session["FirstName"] = data.FirstOrDefault().first_name;
                    Session["Email"] = data.FirstOrDefault().email;
                    Session["idUser"] = data.FirstOrDefault().id_user;
                    FormsAuthentication.SetAuthCookie(data.FirstOrDefault().first_name, false);
                    if (email == "admin@wp.pl" && password == "admin123")
                    {
                        return RedirectToAction("Index", "Admin");
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {

                    ModelState.AddModelError("password", "Sprawdź poprawność wprowadzanych danych");

                }
            }
            return View();
        }
        [Authorize]
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult UserPage(int? err)
        {
            if (err.HasValue)
            {
                ViewBag.tooMuchAdv = "true";
            }
            string idUser;
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            else
            {
                return RedirectToAction("Login");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            UsersModels userModel = db.Users.FirstOrDefault(y => y.id_user == idUserInt);

            int numberAdvAll = db.Advertisment.Where(y => y.id_user == idUserInt).Count();
            int numberAdvActive = db.Advertisment.Where(y => y.id_user == idUserInt && y.active == true).Count();
            ViewBag.numberAdvAll = numberAdvAll;
            ViewBag.numberAdvActive = numberAdvActive;
            return View(userModel);
        }

        [Authorize]
        public ActionResult EditUserData()
        {
            string idUser;
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            else
            {
                return RedirectToAction("Login");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            UsersModels userModel = db.Users.FirstOrDefault(y => y.id_user == idUserInt);
            return View(userModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUserData([Bind(Include = "id_user,first_name,city,phone")] UsersModels user)
        {
            if (user.first_name != null && user.city != null && user.phone.ToString() != "")
            {
                UsersModels userModel = db.Users.FirstOrDefault(y => y.id_user == user.id_user);

                userModel.first_name = user.first_name;
                userModel.city = user.city;
                userModel.phone = user.phone;
                userModel.ConfirmPassword = userModel.password;
                try
                {
                    db.Entry(userModel).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
                return RedirectToAction("UserPage");
            }
            else
            {
                return View(user);
            }
        }
        [Authorize]
        public ActionResult EditPassword()
        {

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPassword([Bind(Include = "id_user,newPassword,password,ConfirmPassword")] UsersModels user)
        {
            if (user.newPassword != null && user.password != null && user.ConfirmPassword != null)
            {
                string idUser;
                if (Session["idUser"] != null)
                {
                    idUser = Session["idUser"].ToString();
                }
                else
                {
                    return RedirectToAction("Login");
                }
                int idUserInt;
                Int32.TryParse(idUser, out idUserInt);
                UsersModels userModel = db.Users.FirstOrDefault(y => y.id_user == idUserInt);

                var f_password = GetMD5(user.newPassword);
                var f_passwordOld = GetMD5(user.password);
                if (!userModel.password.Equals(f_passwordOld))
                {
                    ModelState.AddModelError("password", "Hasło nie jest prawidłowe!");
                    return View(user);
                }
                else if (user.newPassword != user.ConfirmPassword)
                {
                    ModelState.AddModelError("ConfirmPassword", "Hasła nie są identyczne!");
                    return View(user);
                }
                userModel.password = f_password;
                userModel.ConfirmPassword = f_password;
                try
                {
                    db.Entry(userModel).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
                return RedirectToAction("PasswordChanged");
            }
            else
            {
                return View(user);
            }
        }
        [Authorize]
        public ActionResult PasswordChanged()
        {
            return View();
        }
        [Authorize]
        public ActionResult PayMoney()
        {

            return View();
        }
        [Authorize]
        public ActionResult ListLikeAdv()
        {
            string idUser;
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            else
            {
                return RedirectToAction("Login");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            List<AdvertisementModels> listAdv = db.Advertisment.Where(y => y.id_user == idUserInt).ToList();
            List<AdvertisementModels> listAdvWithImage = new List<AdvertisementModels>();
            foreach (var item in listAdv)
            {
                CarModels carmodels = db.Car.FirstOrDefault(x => x.id_car == item.id_car);
                ListImagesModels listImageModels = db.ListImages.FirstOrDefault(y => y.id_car == carmodels.id_car);
                ImagesModels imgaemodels = db.Images.FirstOrDefault(y => y.id_image == listImageModels.id_image);
                AdvertisementModels pom = item;

                string imageBase64Data = Convert.ToBase64String(imgaemodels.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                pom.titleImage = imageDataURL;
                listAdvWithImage.Add(pom);
            }
            List<AdvertisementModels> SortedList = listAdvWithImage.OrderBy(o => o.update_date).ToList();
            SortedList.Reverse();
            return View(SortedList);
        }
    }
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string[] users = Users.Split(',');

            if (!httpContext.User.Identity.IsAuthenticated)
                return false;

            if (users.Length > 0 &&
                !users.Contains(httpContext.User.Identity.Name,
                    StringComparer.OrdinalIgnoreCase))
                return false;

            return true;
        }
    }

}
