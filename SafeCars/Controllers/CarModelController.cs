﻿using SafeCars.DataContext;
using SafeCars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SafeCars.Controllers
{
    public class CarModelController : Controller
    {
        private DBDataContext db = new DBDataContext();
        private List<CarsModelsModels> listModels;
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult ChoseMake()
        {
            string idUser;
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            else
            {
                return RedirectToAction("Login");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            int numberAdvActive = db.Advertisment.Where(y => y.id_user == idUserInt && y.active == true).Count();
            if (numberAdvActive >= 3)
            {
                return RedirectToAction("UserPage", "UsersModels", new { err = 3 });
            }
            List<CarMakes> carMakes = db.CarsMakes.ToList();
            return View(carMakes);
        }

        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public string ChoseMake(string idMarks)
        {
            if (idMarks != "undefined")
            {
                Session["idMarks"] = idMarks;
                Session["nameVersion"] = "no have";
                int id = Int32.Parse(idMarks);

                listModels = db.CarsModels.Where(x => x.id_cars_makes == id).ToList();
                return "{\"msg\":\"success\"}";
            }
            return "{\"msg\":\"not\"}";
            // return RedirectToAction("ChoseModel", "CarModel");
        }
        [Authorize]
        public ActionResult ChoseModel() // only verison
        {
            string idMarks = Session["idMarks"].ToString();
            int id = Int32.Parse(idMarks);
            listModels = db.CarsModels.Where(x => x.id_cars_makes == id).ToList();

            if (id == 116)
            {
                CarsModelsModels carModelsModelsOther = new CarsModelsModels();
                carModelsModelsOther.id_cars_models = 2156;
                TempData["car"] = carModelsModelsOther;
                return RedirectToAction("SaveCar", "CarModel", new { car = carModelsModelsOther });

            }
            Int32 index = listModels.Count - 1;
            while (index > 0)
            {
                if (listModels[index].version == listModels[index - 1].version)
                {
                    if (index < listModels.Count - 1)
                        (listModels[index], listModels[listModels.Count - 1]) = (listModels[listModels.Count - 1], listModels[index]);
                    listModels.RemoveAt(listModels.Count - 1);
                    index--;
                }
                else
                    index--;
            }
            index = listModels.Count - 1;
            while (index >= 0)
            {
                if (listModels[index].version == null) listModels.Remove(listModels[index]);
                index--;
            }
            listModels.OrderBy(x => x.version);
            if (listModels.Count == 0) return RedirectToAction("ChoseModelVersion");
            return View(listModels);
        }
        [Authorize]
        [AcceptVerbs(HttpVerbs.Post)]
        public string ChoseModel(string nameVersion)
        {
            Session["nameVersion"] = nameVersion;
            return "{\"msg\":\"success\"}";
        }
        [Authorize]
        public ActionResult ChoseModelVersion()
        {
            string idMarks = Session["idMarks"].ToString();
            string nameVersion = Session["nameVersion"].ToString();
            int idMarksInt = Int32.Parse(idMarks);
            if (nameVersion != "no have" && nameVersion != "Inny")
            {
                listModels = db.CarsModels.Where(x => x.id_cars_makes == idMarksInt && x.version == nameVersion).ToList();
            }
            else
            {
                listModels = db.CarsModels.Where(x => x.id_cars_makes == idMarksInt).ToList();
            }
            listModels.OrderBy(x => x.model);
            return View(listModels);

        }
        [AllowAnonymous]
        public ActionResult SaveCar(CarsModelsModels car)
        {
            if(car == null)
            {
                 CarsModelsModels car2 = TempData["car"] as CarsModelsModels;
                  return RedirectToAction("CreateCar", "Car", new { idModel = car2.id_cars_models });
            }
            return RedirectToAction("CreateCar", "Car", new { idModel = car.id_cars_models });
        }

    }
}