﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SafeCars.DataContext;
using SafeCars.Models;

namespace SafeCars.Controllers
{
    public class AdvertisementController : Controller
    {
        private DBDataContext db = new DBDataContext();
        [AllowAnonymous]
        public ActionResult CreateAdvertisement(int idCar) // po testach bez ?
        {
            Session["idCar"] = idCar.ToString();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdvertisement([Bind(Include = "id_advertisement,id_car,id_user,location,title,location_city,outskirts,description,date_of_issue,update_date,number_of_views,private_ad,price,active,time_of_possession")] AdvertisementModels advertisementModels)
        {
            if (ModelState.IsValid)
            {
                string idCar = Session["idCar"].ToString();
                int idCarInt = Int32.Parse(idCar);
                string idUser = "0";
                try
                {
                    idUser = Session["idUser"].ToString();
                }
                catch (Exception e)
                {
                    return RedirectToAction("Index", "Home");
                }

                int idUserInt = Int32.Parse(idUser);

                advertisementModels.id_car = idCarInt;
                advertisementModels.id_user = idUserInt;
                advertisementModels.date_of_issue = DateTime.Now;
                advertisementModels.update_date = DateTime.Now;
                advertisementModels.active = true;
                advertisementModels.number_of_views = 0;
                advertisementModels.number_of_vin = 0;
                db.Advertisment.Add(advertisementModels);
                db.SaveChanges();

                return RedirectToAction("AddImage");
            }
            return View(advertisementModels);
        }
        [AllowAnonymous]
        public ActionResult AddLocation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AddImage()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult AddImage(ImagesModels model)
        {
            HttpPostedFileBase file = Request.Files["myfile"];

            if (file != null)
            {

                model.img = new byte[file.ContentLength];
                file.InputStream.Read(model.img, 0, file.ContentLength);
                db.Images.Add(model);
                db.SaveChanges();

                ImagesModels idImg = db.Images.Where(x => x.img == model.img && x.id_image == model.id_image).FirstOrDefault();
                string idCar = Session["idCar"].ToString();
                int idCarInt = Int32.Parse(idCar);
                ListImagesModels newRecord = new ListImagesModels();
                newRecord.id_image = idImg.id_image;
                newRecord.id_car = idCarInt;
                db.ListImages.Add(newRecord);
                db.SaveChanges();

                List<ListImagesModels> listIdFoto = db.ListImages.Where(x => x.id_car == idCarInt).ToList();
                List<ImagesModels> listFoto = new List<ImagesModels>();
                foreach (var item in listIdFoto)
                {
                    listFoto.Add(db.Images.Where(x => x.id_image == item.id_image).FirstOrDefault());

                }
                List<Image> listImage = new List<Image>();
                List<string> listImageString = new List<string>();
                foreach (var item in listFoto)
                {
                    string imageBase64Data = Convert.ToBase64String(item.img);
                    string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                    listImageString.Add(imageDataURL);
                }

                ViewBag.listAllFoto = listImageString;
                return View();
            }
            else
            {
                return View();
            }

        }

        [AllowAnonymous]
        public ActionResult AdAdded()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AdvertisementView(int id) 
        {
             if (Session["idUser"] != null)
            {
                ViewBag.UserIs = Session["idUser"].ToString();
            }
            else
            {
                 ViewBag.UserIs = "-1";
            }
            AdvertisementModels advModel = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            advModel.number_of_views = advModel.number_of_views + 1;
            try
            {
                db.Entry(advModel).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            CarModels carModel = db.Car.FirstOrDefault(y => y.id_car == advModel.id_car);
            List<ListImagesModels> listImageId = db.ListImages.Where(y => y.id_car == advModel.id_car).ToList();
            List<ImagesModels> listImage = new List<ImagesModels>();
            foreach (var item in listImageId)
            {
                listImage.Add(db.Images.FirstOrDefault(y => y.id_image == item.id_image));
            }
            List<string> listImageString = new List<string>();
            foreach (var item in listImage)
            {
                string imageBase64Data = Convert.ToBase64String(item.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                listImageString.Add(imageDataURL);
            }
            UsersModels user = db.Users.FirstOrDefault(y => y.id_user == advModel.id_user);
            CarsModelsModels carModelName = db.CarsModels.FirstOrDefault(y => y.id_cars_models == carModel.id_cars_models);
            CarMakes carMakes = db.CarsMakes.FirstOrDefault(y => y.id_cars_makes == carModelName.id_cars_makes);
            carModelName.carMakes = carMakes;
            carModel.carModels = carModelName;
            advModel.usersModels = user;
            AdvCarImg model = new AdvCarImg();
            model.advModel = advModel;
            model.carModel = carModel;
            model.imgList = listImageString;
            ViewBag.count = 0;

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public string likeButton(String adv) 
        {
            int pom;
            WatchListModels watchListModels = new WatchListModels();
            Int32.TryParse(adv, out pom);
            watchListModels.id_advertisement = pom;
            string idUser = "-1";


            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            if (idUser == "-1")
            {
                return "{\"msg\":\"notIdUser\"}";
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            watchListModels.id_user = idUserInt;

            WatchListModels check = db.WatchList.FirstOrDefault(y => y.id_advertisement == watchListModels.id_advertisement && watchListModels.id_user == y.id_user);
            if (check == null)
            {
                db.WatchList.Add(watchListModels);
                db.SaveChanges();
                return "{\"msg\":\"add\"}";
            }
            else
            {
                ViewBag.messlikebutton = "Usunięto";
                db.WatchList.Remove(check);
                db.SaveChanges();
                return "{\"msg\":\"delete\"}";
            }
        }
        public ActionResult ActivateAdv(int id) 
        {
            string idUser = "-1";
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            if (idUser == "-1")
            {
                return RedirectToAction("Login", "UsersModels");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            int count = db.Advertisment.Where(y => y.id_user == idUserInt && y.active == true).Count();
            if (count >= 3)
            {
                return RedirectToAction("UserPage", "UsersModels", new { err = 3 });
            }
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);

            adv.active = true;
            try
            {
                db.Entry(adv).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return View();
        }
        public ActionResult NotActivateAdv(int id) //zmiien to
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            adv.active = false;
            try
            {
                db.Entry(adv).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public string countVin(String adv)
        {
            int advId;
            Int32.TryParse(adv, out advId);

            AdvertisementModels advM = db.Advertisment.FirstOrDefault(y => y.id_advertisement == advId);
            advM.number_of_vin = advM.number_of_vin + 1;
            try
            {
                db.Entry(advM).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return "{\"msg\":\"add\"}";
        }
        [AllowAnonymous]
        public ActionResult WatchedAdv()
        {
            string idUser = "-1";
            if (Session["idUser"] != null)
            {
                idUser = Session["idUser"].ToString();
            }
            if (idUser == "-1")
            {
                return RedirectToAction("Login", "UsersModels");
            }
            int idUserInt;
            Int32.TryParse(idUser, out idUserInt);
            List<WatchListModels> watchList = db.WatchList.Where(y => y.id_user == idUserInt).ToList();
            List<AdvertisementModels> listAdv = new List<AdvertisementModels>();
            foreach (var item in watchList)
            {
                
                listAdv.Add(db.Advertisment.FirstOrDefault(y => y.id_advertisement == item.id_advertisement ));
            }
            listAdv.RemoveAll(y=>y.active == false);

            List<AdvertisementModels> listAdvWithImage = new List<AdvertisementModels>();
            foreach (var item in listAdv)
            {
                CarModels carmodels = db.Car.FirstOrDefault(x => x.id_car == item.id_car);
                ListImagesModels listImageModels = db.ListImages.FirstOrDefault(y => y.id_car == carmodels.id_car);
                ImagesModels imgaemodels = db.Images.FirstOrDefault(y => y.id_image == listImageModels.id_image);
                AdvertisementModels pom = item;

                string imageBase64Data = Convert.ToBase64String(imgaemodels.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                pom.titleImage = imageDataURL;
                listAdvWithImage.Add(pom);
            }
            return View(listAdvWithImage);
        }
       [Authorize]
        public ActionResult ChangePrice(int id)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            return View(adv);
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePrice([Bind(Include = "id_advertisement,price")] AdvertisementModels advertisementModels)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == advertisementModels.id_advertisement);
            adv.price = advertisementModels.price;
            try
            {
                db.Entry(adv).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                return View();
            }
            return RedirectToAction("ListLikeAdv", "UsersModels");
        }
        [AllowAnonymous]
        public string DeleteAdv(int id)
        {
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            db.WatchList.RemoveRange(db.WatchList.Where(x => x.id_advertisement == id));
            List<ListImagesModels> listImage = db.ListImages.Where(y => y.id_car == adv.id_car).ToList();
            foreach (var item in listImage)
            {
                db.Images.RemoveRange(db.Images.Where(y => y.id_image == item.id_image));
            }
            db.ListImages.RemoveRange(listImage);
            db.Car.RemoveRange(db.Car.Where(y => y.id_car == adv.id_car));
            db.ListOfAds.RemoveRange(db.ListOfAds.Where(y => y.id_advertisement == id));
            db.Complaints.RemoveRange(db.Complaints.Where(y => y.id_advertisement == id));

            db.Advertisment.Remove(adv);
            db.SaveChanges();
            return "{\"msg\":\"success\"}";
        }
        [Authorize]
        public ActionResult EditAdv(int id)
        {
            AdvertisementModels adv = db.Advertisment.Find(id);
            UsersModels user = db.Users.Find(adv.id_user);
            user.ban_message = "empty";
            user.ConfirmPassword = user.password;
            try
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return View(adv);
        }
        [Authorize]
        [HttpGet]
        public ActionResult EditImg(int id)
        {

            AdvertisementModels adv = db.Advertisment.Find(id);
            List<ListImagesModels> listIdFoto = db.ListImages.Where(x => x.id_car == adv.id_car).ToList();
            List<ImagesModels> listFoto = new List<ImagesModels>();
            Session["idCar"] = adv.id_car;
            foreach (var item in listIdFoto)
            {
                listFoto.Add(db.Images.Where(x => x.id_image == item.id_image).FirstOrDefault());

            }
            List<Image> listImage = new List<Image>();
            List<string> listImageString = new List<string>();
            foreach (var item in listFoto)
            {
                string imageBase64Data = Convert.ToBase64String(item.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                listImageString.Add(imageDataURL);
            }
            ViewBag.idAdv = id;
            ViewBag.listAllFoto = listImageString;
            return View();
        }
        public string DeleteImg(string el)
        {
            List<ImagesModels> imageModels = db.Images.ToList();
            ImagesModels toDelete = new ImagesModels();
            foreach (var item in imageModels)
            {

                string imageBase64Data = Convert.ToBase64String(item.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                if (imageDataURL == el)
                {
                    toDelete = item;
                    ListImagesModels listImageToDelete = db.ListImages.FirstOrDefault(y => y.id_image == toDelete.id_image);
                    db.ListImages.Remove(listImageToDelete);
                    db.Images.Remove(toDelete);
                    db.SaveChanges();
                    return "{\"msg\":\"success\"}";

                }
            }

            return "{\"msg\":\"Not\"}";

        }
        [Authorize]
        [HttpPost]
        public ActionResult EditImg(ImagesModels model)
        {
            HttpPostedFileBase file = Request.Files["myfile"];

            if (file != null)
            {

                model.img = new byte[file.ContentLength];
                file.InputStream.Read(model.img, 0, file.ContentLength);
                db.Images.Add(model);
                db.SaveChanges();

                ImagesModels idImg = db.Images.Where(x => x.img == model.img && x.id_image == model.id_image).FirstOrDefault();
                string idCar = Session["idCar"].ToString();
                int idCarInt = Int32.Parse(idCar);
                ListImagesModels newRecord = new ListImagesModels();
                newRecord.id_image = idImg.id_image;
                newRecord.id_car = idCarInt;
                db.ListImages.Add(newRecord);
                db.SaveChanges();

                List<ListImagesModels> listIdFoto = db.ListImages.Where(x => x.id_car == idCarInt).ToList();
                List<ImagesModels> listFoto = new List<ImagesModels>();
                foreach (var item in listIdFoto)
                {
                    listFoto.Add(db.Images.Where(x => x.id_image == item.id_image).FirstOrDefault());

                }
                List<Image> listImage = new List<Image>();
                List<string> listImageString = new List<string>();
                foreach (var item in listFoto)
                {
                    string imageBase64Data = Convert.ToBase64String(item.img);
                    string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                    listImageString.Add(imageDataURL);
                }

                ViewBag.listAllFoto = listImageString;
              
                return View();
            }
            else
            {
                return View();
            }

        }
        public ActionResult EditParameter(int id)
        {
            AdvertisementModels adv = db.Advertisment.Find(id);
            TempData["adv"] =adv;
            return View(adv);
        }

        [HttpPost]
        public ActionResult EditParameter([Bind(Include = "id_advertisement,id_car,id_user,location,title,location_city,outskirts,description,date_of_issue,update_date,number_of_views,private_ad,price,active,time_of_possession")] AdvertisementModels advertisementModels)
        {
            AdvertisementModels advPom = TempData["adv"] as AdvertisementModels;
            advPom.location = advertisementModels.location;
            advPom.title = advertisementModels.title;
            advPom.location_city = advertisementModels.location_city;
            advPom.description = advertisementModels.description;
            advPom.outskirts = advertisementModels.outskirts;
            advPom.private_ad= advertisementModels.private_ad;
            advPom.price = advertisementModels.price;
            advPom.time_of_possession =advertisementModels.time_of_possession;
            advertisementModels.update_date = DateTime.Now;
            try
            {
                db.Entry(advPom).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }

            return RedirectToAction("AdvertisementView", "Advertisement", new { id = advPom.id_advertisement });
        }

       
        // POST: Advertisement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AdvertisementModels advertisementModels = db.Advertisment.Find(id);
            db.Advertisment.Remove(advertisementModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
