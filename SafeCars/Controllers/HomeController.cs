﻿using SafeCars.DataContext;
using SafeCars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace SafeCars.Controllers
{
    public class HomeController : Controller
    {
        private DBDataContext db = new DBDataContext();
        [AllowAnonymous]
        public ActionResult Index()
        {
            List<CarMakes> listCarMakes = db.CarsMakes.ToList();
            List<string> listCarMakesString = new List<string>();
            foreach (var item in listCarMakes)
            {
                listCarMakesString.Add(item.name_make);
            }
            ViewBag.listCarMakes = listCarMakesString;

            List<AdvertisementModels> listAdv = db.Advertisment.Where(y => y.active == true).ToList();
            List<AdvertisementModels> listAdvWithImage = new List<AdvertisementModels>();
            foreach (var item in listAdv)
            {
                CarModels carmodels = db.Car.FirstOrDefault(x => x.id_car == item.id_car);
                ListImagesModels listImageModels = db.ListImages.FirstOrDefault(y => y.id_car == carmodels.id_car);
                ImagesModels imgaemodels = db.Images.FirstOrDefault(y => y.id_image == listImageModels.id_image);
                AdvertisementModels pom = item;

                string imageBase64Data = Convert.ToBase64String(imgaemodels.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                pom.titleImage = imageDataURL;
                listAdvWithImage.Add(pom);
            }
            
            List<AdvertisementModels> SortedList = listAdvWithImage.OrderBy(o=>o.update_date).ToList();
            SortedList.Reverse();
            return View(SortedList);
        }
        [AllowAnonymous]
        public JsonResult Method(string id)
        {
            CarMakes car = db.CarsMakes.FirstOrDefault(y => y.name_make == id); // to nazwa 
            List<CarsModelsModels> carModels = db.CarsModels.Where(y => y.id_cars_makes == car.id_cars_makes).ToList();
            List<string> listModelsString = new List<string>();
            foreach (var item in carModels)
            {
                listModelsString.Add(item.model);
            }

            return Json(new JsonResult()
            {
                Data = listModelsString
            }, JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(string selectMake, string selectModel, string yearStart, string yearEnd, string prizeStart, string prizeEnd, string pojStart, string pojEnd, string przStart, string przEnd, string powStart, string powEnd, string oli, string tran, string colorChoice, string cityChoice, string wojChoice, string dema, string newCar)
        {
            List<CarMakes> listCarMakes = db.CarsMakes.ToList();
            List<string> listCarMakesString = new List<string>();
            foreach (var item in listCarMakes)
            {
                listCarMakesString.Add(item.name_make);
            }
            ViewBag.listCarMakes = listCarMakesString;

            List<AdvertisementModels> listAdv = db.Advertisment.Where(y => y.active == true).ToList();
            List<AdvertisementModels> listAdvWithImage = new List<AdvertisementModels>();
            foreach (var item in listAdv)
            {
                CarModels carmodels = db.Car.FirstOrDefault(x => x.id_car == item.id_car);
                ListImagesModels listImageModels = db.ListImages.FirstOrDefault(y => y.id_car == carmodels.id_car);
                ImagesModels imgaemodels = db.Images.FirstOrDefault(y => y.id_image == listImageModels.id_image);

                AdvertisementModels pom = item;
                CarsModelsModels carModelsModels = db.CarsModels.FirstOrDefault(y => y.id_cars_models == carmodels.id_cars_models);
                CarMakes carMake = db.CarsMakes.FirstOrDefault(y => y.id_cars_makes == carModelsModels.id_cars_makes);
                carModelsModels.carMakes = carMake;
                carmodels.carModels = carModelsModels;
                pom.carModels = carmodels;
                string imageBase64Data = Convert.ToBase64String(imgaemodels.img);
                string imageDataURL = string.Format("data:image/png;base64,{0}", imageBase64Data);
                pom.titleImage = imageDataURL;
                listAdvWithImage.Add(pom);
            }
            if (selectMake != "Marka") listAdvWithImage.RemoveAll(y => y.carModels.carModels.carMakes.name_make != selectMake);
            if (selectModel != "Model") listAdvWithImage.RemoveAll(y => y.carModels.carModels.model != selectModel);
            if (!String.IsNullOrEmpty(yearStart) || !String.IsNullOrEmpty(yearEnd))
            {
                int start, end;
                if (Int32.TryParse(yearStart, out start)) listAdvWithImage.RemoveAll(y => y.carModels.year < start);
                if (Int32.TryParse(yearEnd, out end)) listAdvWithImage.RemoveAll(y => y.carModels.year > end);
            }
            if (!String.IsNullOrEmpty(prizeStart) || !String.IsNullOrEmpty(prizeEnd))
            {
                int start, end;
                if (Int32.TryParse(prizeStart, out start)) listAdvWithImage.RemoveAll(y => y.price < start);
                if (Int32.TryParse(prizeEnd, out end)) listAdvWithImage.RemoveAll(y => y.price > end);
            }
            if (!String.IsNullOrEmpty(pojStart) || !String.IsNullOrEmpty(pojEnd))
            {
                int start, end;
                if (Int32.TryParse(pojStart, out start)) listAdvWithImage.RemoveAll(y => y.carModels.capacity < start);
                if (Int32.TryParse(pojEnd, out end)) listAdvWithImage.RemoveAll(y => y.carModels.capacity > end);
            }
            if (!String.IsNullOrEmpty(przStart) || !String.IsNullOrEmpty(przEnd))
            {
                int start, end;
                if (Int32.TryParse(przStart, out start)) listAdvWithImage.RemoveAll(y => y.carModels.mileage < start);
                if (Int32.TryParse(przEnd, out end)) listAdvWithImage.RemoveAll(y => y.carModels.mileage > end);
            }
            if (!String.IsNullOrEmpty(powStart) || !String.IsNullOrEmpty(powEnd))
            {
                int start, end;
                if (Int32.TryParse(powStart, out start)) listAdvWithImage.RemoveAll(y => y.carModels.power < start);
                if (Int32.TryParse(powEnd, out end)) listAdvWithImage.RemoveAll(y => y.carModels.power > end);
            }
            if (oli != "Paliwo") listAdvWithImage.RemoveAll(y => y.carModels.fuel != oli);
            if (tran != "Skrzynia") listAdvWithImage.RemoveAll(y => y.carModels.transmission != tran);
            if (!String.IsNullOrEmpty(colorChoice)) listAdvWithImage.RemoveAll(y => y.carModels.color != colorChoice);
            if (!String.IsNullOrEmpty(cityChoice)) listAdvWithImage.RemoveAll(y => y.location_city != cityChoice);
            if (wojChoice != "Województwo") listAdvWithImage.RemoveAll(y => y.location != wojChoice);
            if (!String.IsNullOrEmpty(dema))
            {
                if (dema == "TAK")
                    listAdvWithImage.RemoveAll(y => y.carModels.damaged == false);
                if (dema == "NIE")
                    listAdvWithImage.RemoveAll(y => y.carModels.damaged == true);
            }
            if (!String.IsNullOrEmpty(newCar))
            {
                if (newCar == "TAK")
                    listAdvWithImage.RemoveAll(y => y.carModels.used == true);
                if (newCar == "NIE")
                    listAdvWithImage.RemoveAll(y => y.carModels.used == false);
            }
              List<AdvertisementModels> SortedList = listAdvWithImage.OrderBy(o=>o.update_date).ToList();
            
            SortedList.Reverse();
            return View(SortedList);


        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}