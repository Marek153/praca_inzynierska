﻿using SafeCars.DataContext;
using SafeCars.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SafeCars.Controllers
{
    public class ComplaintsController : Controller
    {
        private DBDataContext db = new DBDataContext();
        [HttpGet]
        [Authorize]
        public ActionResult SendComplaints(int id)
        {

            ComplaintsModels complaintModel = new ComplaintsModels();
            AdvertisementModels adv = db.Advertisment.FirstOrDefault(y => y.id_advertisement == id);
            complaintModel.id_advertisement = id;
            complaintModel.advertisement = adv;
            return View(complaintModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult SendComplaints(ComplaintsModels complaint)
        {
            ComplaintsModels complaintModel = new ComplaintsModels();
            complaintModel.id_advertisement = complaint.id_advertisement;
            complaintModel.description = complaint.description;
            if (Session["idUser"] == null)
            {
                return RedirectToAction("Login", "UsersModels");
            }
            string idUser = "-1";
            try
            {
                idUser = Session["idUser"].ToString();
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Home");
            }
            int idUserInt = Int32.Parse(idUser);
            complaintModel.id_user = idUserInt;
            db.Complaints.Add(complaintModel);
            db.SaveChanges();
            return RedirectToAction("AdvertisementView", "Advertisement", new { id = complaint.id_advertisement });
        }
    }
}