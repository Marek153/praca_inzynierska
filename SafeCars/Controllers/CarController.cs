﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SafeCars.DataContext;
using SafeCars.ExternalApi;
using SafeCars.Models;

namespace SafeCars.Controllers
{
    public class CarController : Controller
    {
        private DBDataContext db = new DBDataContext();

        [AllowAnonymous]
        public ActionResult CreateCar(int? idModel)
        {
            Session["idModel"] = idModel.ToString();
            return View();

        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCar([Bind(Include = "id_car,id_cars_models,year,mileage,capacity,power,fuel,transmission,vin,color,used,damaged,first_registration,license_plate")] CarModels carModels)
        {
            if (ModelState.IsValid)
            {
                string idModel = Session["idModel"].ToString();
                int idModelInt =Int32.Parse(idModel);
                carModels.id_cars_models=idModelInt;
                db.Car.Add(carModels);
                db.SaveChanges();
                List<CarModels>onlyForId = db.Car.Where(x => x.id_car==carModels.id_car).ToList();
                int pomId= onlyForId.First().id_car;
                if (onlyForId==null)  return View(carModels);
                return RedirectToAction("CreateAdvertisement", "Advertisement", new { idCar = pomId });
            }
            return View(carModels);
        }



        // POST: Car/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarModels carModels = db.Car.Find(id);
            db.Car.Remove(carModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
