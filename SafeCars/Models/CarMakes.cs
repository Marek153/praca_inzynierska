﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
 
    [Table("car_makes", Schema = "public")]
    public class CarMakes
    {
        [Key]
        public int id_cars_makes { get; set; }
        [DisplayName("Marka")]
        public string name_make { get; set; }
      
    }
   
}