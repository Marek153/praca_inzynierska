﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("car", Schema = "public")]
    public class CarModels
    {
        [Key]
        public int id_car { get; set; }
        [ForeignKey("carModels")]
        public int id_cars_models { get; set; }
        public CarsModelsModels carModels { get; set; }
        [Range(1800, 2021, ErrorMessage = "Rok produkcji musi być z przedziału 1800-2020")]
        [DisplayName("Rok produkcji")]
        public int year { get; set; }
        [DisplayName("Pojemność")]
        public int capacity { get; set; }
        [DisplayName("Przebieg")]
        public int mileage { get; set; }
        [DisplayName("Moc")]
        public int power { get; set; }
        [DisplayName("Rodzaj paliwa")]
        public string fuel { get; set; }
        [DisplayName("Skrzynia biegów")]
        public string transmission { get; set; }
        [DisplayName("Vin")]
        public string vin { get; set; }
        [DisplayName("Kolor")]
        public string color { get; set; }
        [DisplayName("Używany?")]
        public Boolean used { get; set; }
        [DisplayName("Uszkodzony?")]
        public Boolean damaged { get; set; }
        [DisplayName("Data pierwszej rejestracji")]
        public DateTime first_registration { get; set; }
        [DisplayName("Obecena tablica rejestracyjna")]
        public string license_plate { get; set; }
    }
    public enum FuelCar
    {
        Diesel,
        Benzyna,
        LPG,
        Hybryda,
        Elektryczny
    }
    public enum TransmissionCar
    {
        Manualna,
        Automatyczna
    }

}