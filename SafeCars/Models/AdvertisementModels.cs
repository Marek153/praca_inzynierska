﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("advertisement", Schema = "public")]
    public class AdvertisementModels
    {
        [Key]
        public int id_advertisement { get; set; }
        [ForeignKey("carModels")]
        public int id_car { get; set; }
        public CarModels carModels { get; set; }

        [ForeignKey("usersModels")]
        public int id_user { get; set; }
        public UsersModels usersModels { get; set; }
        [DisplayName("Województwo")]
        public string location { get; set; }
        [DisplayName("Miasto")]
        public string location_city { get; set; }
        [DisplayName("Czy pojazd znajduje się poza granicami wskazanego miasta?")]
        public Boolean outskirts { get; set; }

        [DisplayName("Tytuł")]
        public string title { get; set; }
        [DisplayName("Opis")]
        public string description { get; set; }
        public DateTime date_of_issue { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMM-yyyy}")]
        public DateTime update_date { get; set; }
        public int number_of_views { get; set; }
         public int number_of_vin{ get; set; }
        [DisplayName("Czy ogłszoenie jest od osoby prywatnej?")]
        public Boolean private_ad { get; set; }
        [DisplayName("Cena")]
        public int price { get; set; }
        public Boolean active { get; set; }
        [DisplayName("Jak długo posiadasz samochód?")]
        [DisplayFormat(DataFormatString = "{0:MMM-yyyy}")]
        public DateTime time_of_possession { get; set; }
        [NotMapped]
        public string titleImage { get; set; }
    }
    public enum Voivodeships
    {
        dolnośląskie,
        [Description("kujawsko-pomorskie")]
        kujawsko_pomorskie,
        lubelskie,
        lubuskie,
        łódzkie,
        małopolskie,
        mazowieckie,
        opolskie,
        podkarpackie,
        podlaskie,
        pomorskie,
        śląskie,
        świętokrzyskie,
        [Description(" warmińsko-mazurskie")]
        warmińsko_mazurskie,
        wielkopolskie,
        zachodniopomorskie
    }
}