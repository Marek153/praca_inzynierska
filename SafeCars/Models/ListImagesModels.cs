﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("list_images ", Schema = "public")]
    public class ListImagesModels
    {
        [Key]
        [Column(Order = 1)]
        public int id_image { get; set; }
        public ImagesModels imagesModels { get; set; }

        [Key]
        [Column(Order = 2)]
        public int id_car { get; set; }
        public CarModels carModels { get; set; }

    }
}