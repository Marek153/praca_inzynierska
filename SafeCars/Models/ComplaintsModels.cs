﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("complaints", Schema = "public")]
    public class ComplaintsModels
    {
        [Key]
        public int id_complaints { get; set; }
        [ForeignKey("advertisement")]
        public int id_advertisement { get; set; }
        public AdvertisementModels advertisement { get; set; }
        [DisplayName("Treść skargi")]
        public string description { get; set; }
        [ForeignKey("usersModels")]
        public int id_user { get; set; }
        public UsersModels usersModels { get; set; }


    }
}