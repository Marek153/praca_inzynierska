﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("images", Schema = "public")]
    public class ImagesModels
    {
        [Key]
        public int id_image { get; set; }
      
       [MaxLength]
        public byte[] img { get; set; }
    }
}