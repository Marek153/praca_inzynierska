﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [NotMapped]
    public class MakeandModel
    {
        public CarMakes carMake { get; set; }
        public CarsModelsModels carModel { get; set; }
    }
}