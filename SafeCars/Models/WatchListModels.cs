﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("watchlist", Schema = "public")]
    public class WatchListModels
    {
        [Key]
        [Column(Order = 1)]
        public int id_advertisement { get; set; }
        public AdvertisementModels advertisementModels { get; set; }
        [Key]
        [Column(Order = 2)]
        public int id_user { get; set; }

        public UsersModels usersModels { get; set; }
    }
}