﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("users", Schema = "public")]
    public class UsersModels
    {
        [Key]
        public int id_user { get; set; }
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Hasło")]
        public string password { get; set; }
        [NotMapped]
        [Required]
        [DisplayName("Powtórz hasło")]
        public string ConfirmPassword { get; set; }
        [NotMapped]
        public string newPassword { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [DisplayName("Imię")]
        public string first_name { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        [DisplayName("Miasto")]
        public string city { get; set; }
        [RegularExpression(@"^[0-9]{9}$")]
        [DisplayName("Numer telefonu")]
        public long phone { get; set; }
        public string ban_message { get; set; }
    }
}