﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [Table("cars_models", Schema = "public")]
    public class CarsModelsModels
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id_cars_models { get; set; }
        [ForeignKey("carMakes")]
        public int id_cars_makes { get; set; }
        public CarMakes carMakes { get; set; }
        public string model { get; set; }
        public string version { get; set; }
    }
}