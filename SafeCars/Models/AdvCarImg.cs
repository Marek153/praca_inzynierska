﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SafeCars.Models
{
    [NotMapped]
    public class AdvCarImg
    {
        public AdvertisementModels advModel { get; set; }
        public CarModels carModel { get; set; }
        public List<string> imgList { get; set; }


    }
}