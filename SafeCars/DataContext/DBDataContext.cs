﻿using SafeCars.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SafeCars.DataContext
{
    public class DBDataContext : DbContext
    {
        public DBDataContext() : base(nameOrConnectionString: "SafeCarsDatabase")
        {

        }
        public DbSet<AdvertisementModels> Advertisment { get; set; }
        public DbSet<CarMakes> CarsMakes { get; set; }
        public DbSet<CarModels> Car { get; set; }

        public DbSet<CarsModelsModels> CarsModels { get; set; }
        public DbSet<ComplaintsModels> Complaints { get; set; }
        public DbSet<ImagesModels> Images { get; set; }
        public DbSet<ListImagesModels> ListImages { get; set; }
        public DbSet<ListOfAdsModels> ListOfAds { get; set; }
        public DbSet<UsersModels> Users { get; set; }
        public DbSet<WatchListModels> WatchList { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer<demoEntities>(null);
            modelBuilder.Entity<UsersModels>().ToTable("users");
            modelBuilder.Entity<CarsModelsModels>().ToTable("car_models_model");
  
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            base.OnModelCreating(modelBuilder);


        }
        

     
    }
}